/**
 * @file
 */

(function ($, Drupal, once) {

  Drupal.behaviors.formtips = {
    attach: function (context, settings) {

      function hideOnClickOutside(element, $description) {
        var outsideClickListener = function (event) {
          var $target = $(event.target);
          if (!$target.hasClass('formtip') && !$target.hasClass('formtips-processed')) {
            $description.toggleClass('formtips-show', false);
          }
        }

        $(document).on('click', outsideClickListener);
      }

      var formtip_settings = settings.formtips;
      var selectors = formtip_settings.selectors;
      if ($.isArray(selectors)) {
        selectors = selectors.join(', ');
      }

      var $descriptions = $('.form-item .description,.form-item__description,.form-item .filter-guidelines,.fieldset__description')
        .not(selectors)
        .not('.formtips-processed');

      // Filter out empty descriptions. This helps avoid the password strength
      // description getting caught in a help.
      $descriptions = $descriptions.filter(function () {
        return $.trim($(this).text()) !== '';
      });

      if (formtip_settings.max_width.length) {
        $descriptions.css('max-width', formtip_settings.max_width);
      }

      // Hide descriptions when escaped is hit.
      $(document).on('keyup', function (e) {
        if (e.which === 27) {
          $descriptions.removeClass('formtips-show');
        }
      });

      // As in Drupal 10: Remove jQuery dependency from the once feature
      // @see: https://www.drupal.org/node/3158256
      $(once('formtips', $descriptions, context)).each(function () {
        var $description = $(this);
        var $item = $description.closest('.form-item');
        var descriptionId = $description.attr('id');
        var $formtip = $('<a class="formtip" aria-labelledby="' + descriptionId + '" href="javascript:void(0);" tabindex="0"></a>');

        // If there is no description id, skip.
        if (!descriptionId) {
          return;
        }

        if (descriptionId.endsWith('-format-guidelines')) {
          // Grab the field id from guidelines
          fieldId = descriptionId.slice(0, -18) + '-value';
        }
        else {
          // Otherwise grabs it from the field description
          fieldId = descriptionId.substring(0, descriptionId.lastIndexOf("-") - 1);
        }

        // First try to find a label associated with that field
        var $label = $item.find('[for="' + fieldId + '"]:not(.visually-hidden)');

        // Try to get the fieldset from the Field Group module.
        if (!$label.length) {
          $label = $('fieldset.field-group-fieldset[id="' + fieldId + '"] .fieldset__label:not(.visually-hidden)');
        }

        // Try to get the fieldset of the checkboxes element.
        if (!$label.length) {
          $label = $('fieldset.fieldgroup[id="' + fieldId + '"] .fieldset__label:not(.visually-hidden)');
        }

        // Try to get the label that was added as a placeholder.
        if (!$label.length && $('#' + fieldId).attr('placeholder')) {
          $label = $item.find('[for="' + fieldId + '"]');

          if ($label.length && $label.hasClass('visually-hidden')) {
            $label.removeClass('visually-hidden');
          }
        }

        // Check if there is a field with the '-value' suffix.
        if (!$label.length) {
          fieldId = fieldId + '-value';
          $label = $item.find('[for="' + fieldId + '"]:not(.visually-hidden)');
        }

        // Otherwise look for the first label, a fieldset legend or draggable
        // table label.
        if (!$label.length) {
          $label = $item.find('.fieldset-legend,label,.label,.form-item__label').first();

          if ($label.length && $label.hasClass('visually-hidden')) {
            $label.removeClass('visually-hidden');
          }
        }

        // Use the fieldset if the item is a radio or checkbox.
        var $fieldset = $item.find('.fieldset-legend');
        if ($fieldset.length && $item.find('input[type="checkbox"], input[type="radio"]').length) {
          $label = $fieldset;
        }

        // If there is no label, skip.
        if (!$label.length) {
          return;
        }

        $description.addClass('formtips-processed');

        $item.addClass('formtips-item');
        $description.toggleClass('formtips-show', false);
        $description.attr('tabindex', '0');
        $description.attr('role', 'tooltip');
        $label.append($formtip);

        if (formtip_settings.trigger_action === 'click') {
          $formtip.on('click', function () {
            // Filter guidelines use the 'display:none;' style by default.
            if ($description.find('.filter-guidelines-item').length !== 0) {
              $description.children().show();
            }

            $description.toggleClass('formtips-show');
            return false;
          });
          // Hide description when clicking elsewhere.

          hideOnClickOutside($item[0], $description);

        }
        else {
          $formtip.hoverIntent({
            sensitivity: formtip_settings.sensitivity,
            interval: formtip_settings.interval,
            over: function () {
              // Filter guidelines use the 'display:none;' style by default.
              if ($description.find('.filter-guidelines-item').length !== 0) {
                $description.children().show();
              }

              $description.toggleClass('formtips-show', true);
            },
            timeout: formtip_settings.timeout,
            out: function () {
              $description.toggleClass('formtips-show', false);
            }
          });
        }
      });
    }
  };

})(jQuery, Drupal, once);
